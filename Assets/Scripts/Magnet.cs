﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    protected bool containObject = false;

    void  OnTriggerEnter(Collider other)
    {
        if (!containObject)
        {
            if (other.gameObject.tag == "magnet")
            {
                other.gameObject.transform.position = GetComponent<Collider>().transform.position;
                other.transform.rotation = transform.rotation;
                containObject = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (containObject)
        {
            if (other.gameObject.tag == "magnet")
            {
                containObject = false;
            }
        }
    } 
}
