﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brush : MonoBehaviour
{
    public GameObject mixture;
    public GameObject mixtureCollider;
    public float minimumHeight;

    public bool cyan = false;
    public bool magenta = false;
    public bool yellow = false;

    public bool brushEnabled = false;


    // Set the color of the brush
    void Update()
    {
        GetComponent<Renderer>().enabled = brushEnabled;
        if (!cyan && !magenta && !yellow)
        {
            brushEnabled = false;
        }
        if (cyan)
        {
            if (yellow)
            {
                if (magenta)
                {
                    GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0.5f);
                }
                else
                {
                    GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                }
            }
            else
            {
                if (magenta)
                {
                    GetComponent<Renderer>().material.color = new Color(0, 0, 1, 0.5f);
                }
                else
                {
                    GetComponent<Renderer>().material.color = new Color(0, 1, 1, 0.5f);
                }

            }
        }
        else
        {
            if (yellow)
            {
                if (magenta)
                {
                    GetComponent<Renderer>().material.color = new Color(1, 0, 0, 0.5f);
                }
                else
                {
                    GetComponent<Renderer>().material.color = new Color(1, 1, 0, 0.5f);
                }
            }
            else
            {
                if (magenta)
                {
                    GetComponent<Renderer>().material.color = new Color(1, 0, 1, 0.5f);
                }
                else
                {
                    GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.5f);
                }
            }
        }
    } 

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == mixtureCollider)
        {
            brushEnabled = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == mixtureCollider)
        {
                brushEnabled = true;
                cyan = mixture.GetComponent<Mixture>().cyan;
                magenta = mixture.GetComponent<Mixture>().magenta;
                yellow = mixture.GetComponent<Mixture>().yellow;
        }
    }
}
