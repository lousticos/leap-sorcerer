﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colored_symbols : MonoBehaviour
{
    public GameObject sponge;

    public string[] symbolsSetCyan = new string[1];
    public string[] symbolsSetMagenta = new string[1];
    public string[] symbolsSetYellow = new string[1];

    public List<int> symbolCyan;
    public List<int> symbolMagenta;
    public List<int> symbolYellow;

    private bool isActivatedCyan;
    private bool isActivatedMagenta;
    private bool isActivatedYellow;

    public GameObject[] sphere = new GameObject[2];
    public GameObject[] particleSystem = new GameObject[2];

    void OnTriggerExit(Collider other)
    {

        if(other.gameObject.tag == "brush")
        {
            for(int i = 0; i < symbolsSetCyan.Length; i++)
            {              
                //On traite la composante "Cyan" du symbole

                isActivatedCyan = true;

                for (int j = 0; j < symbolsSetCyan[i].Length; j++)
                {
                    symbolCyan.Add((int)char.GetNumericValue(symbolsSetCyan[i][j]));
                }

                for (int j = 0; j < sphere.Length; j++)
                {
                    if (symbolCyan.IndexOf(j) != -1)
                    {
                        isActivatedCyan = isActivatedCyan && sphere[j].GetComponent<TrailDetector>().cyan;
                    }
                    else
                    {
                        isActivatedCyan = isActivatedCyan && !(sphere[j].GetComponent<TrailDetector>().cyan);
                    }
                }
                
                //On traite la composante "Magenta" du symbole

                isActivatedMagenta = true;

                for (int j = 0; j < symbolsSetMagenta[i].Length; j++)
                {
                    symbolMagenta.Add((int)char.GetNumericValue(symbolsSetMagenta[i][j]));
                }

                for (int j = 0; j < sphere.Length; j++)
                {
                    if (symbolMagenta.IndexOf(j) != -1)
                    {
                        isActivatedMagenta = isActivatedMagenta && sphere[j].GetComponent<TrailDetector>().magenta;
                    }
                    else
                    {
                        isActivatedMagenta = isActivatedMagenta && !(sphere[j].GetComponent<TrailDetector>().magenta);
                    }
                }
                

                //On traite la composante "Yellow" du symbole

                isActivatedYellow = true;

                for (int j = 0; j < symbolsSetYellow[i].Length; j++)
                {
                    symbolYellow.Add((int)char.GetNumericValue(symbolsSetYellow[i][j]));
                }

                for (int j = 0; j < sphere.Length; j++)
                {
                    if (symbolYellow.IndexOf(j) != -1)
                    {
                        isActivatedYellow = isActivatedYellow && sphere[j].GetComponent<TrailDetector>().yellow;
                    }
                    else
                    {
                        isActivatedYellow = isActivatedYellow && !(sphere[j].GetComponent<TrailDetector>().yellow);
                    }
                }

                

                //Analyse du symbole
                if (isActivatedCyan && isActivatedMagenta && isActivatedYellow)
                {
                    Debug.Log("Symbole coloré: " + (i + 1));
                    particleSystem[i].gameObject.SetActive(true);
                }
                else
                {
                    particleSystem[i].gameObject.SetActive(false);
                }
                symbolCyan.Clear();
                isActivatedCyan = false;
                symbolMagenta.Clear();
                isActivatedMagenta = false;
                symbolYellow.Clear();
                isActivatedYellow = false;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == sponge)
        {
            for (int j = 0; j < sphere.Length; j++)
            {
                sphere[j].GetComponent<TrailDetector>().cyan = false;
                sphere[j].GetComponent<TrailDetector>().magenta = false;
                sphere[j].GetComponent<TrailDetector>().yellow = false;
            }
            Debug.Log("La pierre a été effacée.");
            symbolCyan.Clear();
            symbolMagenta.Clear();
            symbolYellow.Clear();
            isActivatedCyan = false;
            isActivatedMagenta = false;
            isActivatedYellow = false;
        }
    }
}
