﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSymbol : MonoBehaviour
{
    public GameObject sponge;
    public string[] symbolsSet = new string[1];
    public List<int> symbol;
    private bool isActivated;
    public GameObject[] sphere = new GameObject[2];
    
    void OnTriggerExit(Collider other)
    {

        if(other.gameObject.tag == "brush")
        {
            //Debug.Log("Sortie de la pierre");
            for(int i = 0; i < symbolsSet.Length; i++)
            {
               // Debug.Log("Test symbole " + i);
                isActivated = true;
                for (int j = 0; j < symbolsSet[i].Length; j++)
                {
                    symbol.Add((int)char.GetNumericValue(symbolsSet[i][j]));
                }
                for (int j = 0; j < sphere.Length; j++)
                {
                    //if (sphere[j].GetComponent<TrailDetector>().isTriggered)
                        //Debug.Log("La sphere " + j + "est activee.");
                    //isActivated = isActivated && sphere[(int)char.GetNumericValue(symbolsSet[i][j])].GetComponent<TrailDetector>().isTriggered;
                    if (symbol.IndexOf(j) != -1)
                    {
                        isActivated = isActivated && sphere[j].GetComponent<TrailDetector>().isTriggered;
                    }
                    else
                    {
                        isActivated = isActivated && !(sphere[j].GetComponent<TrailDetector>().isTriggered);
                    }
                }
                if (isActivated)
                {
                    Debug.Log("symbole " + (i+1));
                }
                symbol.Clear();
                isActivated = false;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == sponge)
        {
            for (int j = 0; j < sphere.Length; j++)
            {
                sphere[j].GetComponent<TrailDetector>().isTriggered = false;
            }
            Debug.Log("La pierre a été effacée. ");
            symbol.Clear();
            isActivated = false;
        }
    }
}
