﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand_below : MonoBehaviour
{
    public GameObject leftPalm;
    public GameObject rightPalm;
    public float minimumPalmHeight;

    void Setup()
    {
    }
    void Update()
    {
        if (leftPalm.transform.position.y > minimumPalmHeight && rightPalm.transform.position.y > minimumPalmHeight)
        {
            Color current = GetComponent<Renderer>().material.color;
            GetComponent<Renderer>().material.color = new Color(current.r, current.g, current.b, 1.0f);
        }
        else
        {
            Color current = GetComponent<Renderer>().material.color;
            GetComponent<Renderer>().material.color = new Color(current.r, current.g, current.b, 0.0f);
        }
    }
}
