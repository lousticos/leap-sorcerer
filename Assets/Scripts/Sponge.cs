﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sponge : MonoBehaviour
{

    public GameObject trailInstancer;
    public GameObject pierreCollider;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == pierreCollider)
        {
            foreach (Transform child in trailInstancer.GetComponent<Transform>())
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        if (other.gameObject.tag == "brush")
        {
            other.gameObject.GetComponent<Brush>().brushEnabled = false;
        }
    }
}
