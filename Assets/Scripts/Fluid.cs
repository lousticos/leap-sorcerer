﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fluid : MonoBehaviour
{
    public string color = "yellow";
    public GameObject fiole;
    public GameObject staticFluid;
    public bool invisible = false;

    void Start()
    {
        switch (color)
        {
            case "cyan":
                GetComponent<Renderer>().material.color = new Color(0,1,1,0.5f);
                break;
            case "magenta":
                GetComponent<Renderer>().material.color = new Color(1, 0, 1, 0.5f);
                break;
            case "yellow":
                GetComponent<Renderer>().material.color = new Color(1, 1, 0, 0.5f);
                break;
            default:
                GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.5f);
                break;
        }
        staticFluid.GetComponent<Renderer>().material.color = GetComponent<Renderer>().material.color;
    }

    void Update()
    {
        GetComponent<Renderer>().transform.rotation = Quaternion.identity;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == fiole)
        {
            GetComponent<MeshRenderer>().enabled = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == fiole && !invisible)
        {
            GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
