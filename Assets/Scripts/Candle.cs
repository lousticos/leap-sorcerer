﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Candle : MonoBehaviour
{
    public float seed = 1.0f;
    public float frequency = 1.0f;

    private float amplitude;

    void Start()
    {
        amplitude = GetComponent<Light>().intensity;
    }
    void Update()
    {
        GetComponent<Light>().intensity = amplitude * Mathf.PerlinNoise(frequency * Time.fixedTime, seed); 
    }
}
