﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Devil_hand : MonoBehaviour
{
    public GameObject target;
    public GameObject mainCamera;

    private Vector3 startPos;
    private Quaternion startRot;
    public float transitionTime = 3.0f;

    private Vector3 endPos;
    private Quaternion endRot;
    private float currentTime;
    public bool activated = false;

    void Start()
    {
        startPos = transform.position;
        startRot = transform.rotation;

        endPos = target.transform.position;
        endRot = transform.rotation;

    }
    
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > transitionTime)
        {
            mainCamera.transform.SetParent(GetComponent<Transform>());
            transform.position = Vector3.Lerp(endPos, startPos, (currentTime - transitionTime) / transitionTime);
            transform.rotation = Quaternion.Slerp(endRot, startRot, (currentTime - transitionTime) / transitionTime);
        }
        else
        {
            transform.position = Vector3.Lerp(startPos, endPos, currentTime / transitionTime);
            transform.rotation = Quaternion.Slerp(startRot, endRot, currentTime / transitionTime);
        }
    }
}
