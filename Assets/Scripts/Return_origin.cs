﻿using System.Collections;
using System.Collections.Generic;
using Leap.Unity.Interaction;
using UnityEngine;

public class Return_origin : MonoBehaviour
{
    public float transformationTime = 3.0f;
    private float currentTime = 0.0f;
    private bool holded = false;

    public void OnGraspEnd()
    {
        currentTime = 0.0f;
        holded = false;
        /*   
        transform.position = originPosition;
        transform.rotation = originRotation;
        */
        
    }

    public void OnGraspEnter()
    {
        holded = true;
    }

    public void Update()
    {
        if(!holded)
        {
            currentTime += Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, this.GetComponent<Minimum_y>().nonGrabbedPosition, currentTime/transformationTime);
            transform.rotation = Quaternion.Slerp(transform.rotation, this.GetComponent<Minimum_y>().nonGrabbedRotation, currentTime / transformationTime);
        }
    }
}
