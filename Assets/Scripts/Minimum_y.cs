﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimum_y : MonoBehaviour
{
    public float minimum_y = 0.0f;
    public Vector3 nonGrabbedPosition;
    public Quaternion nonGrabbedRotation;
    public bool movable = true;

    void Start()
    {
        if (GetComponent<Transform>().position.y < minimum_y)
        {
            GetComponent<Transform>().position = new Vector3(GetComponent<Transform>().transform.position.x, minimum_y, GetComponent<Transform>().transform.position.z);
            GetComponent<Transform>().rotation = Quaternion.identity;
        }
        nonGrabbedPosition = this.transform.position;
        nonGrabbedRotation = this.transform.rotation;
    }

    void Update()
    {
        if(GetComponent<Transform>().position.y < minimum_y)
        {
            GetComponent<Transform>().position = new Vector3(GetComponent<Transform>().transform.position.x, minimum_y, GetComponent<Transform>().transform.position.z);
            //GetComponent<Transform>().rotation = Quaternion.identity;

            if (movable)
            {
                nonGrabbedPosition = this.transform.position;
                nonGrabbedRotation = Quaternion.identity;
            }
        }
    }
}
