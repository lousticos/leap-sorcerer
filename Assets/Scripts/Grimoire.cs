﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grimoire : MonoBehaviour
{
    public GameObject leftTrigger;
    public GameObject rightTrigger;
    public GameObject[] pages = new GameObject[5];
    
    private int currentPage;
    

    protected string activatedFirst = "none";

    void Start()
    {
        currentPage = 0;
    }

    void Update()
    {
        for(int i = 0; i < pages.Length; i++)
        {
            if(currentPage == i)
            {
                pages[i].GetComponent<Renderer>().enabled = true;
            }
            else
            {
                pages[i].GetComponent<Renderer>().enabled = false;
            }
        }
        if (leftTrigger.GetComponent<Grimoire_trigger>().isActivated && !rightTrigger.GetComponent<Grimoire_trigger>().isActivated)
        {
            activatedFirst = "left";    
        }

        if (rightTrigger.GetComponent<Grimoire_trigger>().isActivated && !leftTrigger.GetComponent<Grimoire_trigger>().isActivated)
        {
            activatedFirst = "right";    
        }

        if (rightTrigger.GetComponent<Grimoire_trigger>().isActivated && leftTrigger.GetComponent<Grimoire_trigger>().isActivated)
        {
            if(activatedFirst == "left")
            {
                Debug.Log("Page précédente");
                currentPage -= 1;
                if(currentPage < 0)
                {
                    currentPage = 0;
                }
            }
            else if(activatedFirst == "right")
            {
                Debug.Log("Page suivante");
                currentPage += 1;
                if (currentPage > pages.Length - 1)
                {
                    currentPage = pages.Length - 1;
                }
            }
            rightTrigger.GetComponent<Grimoire_trigger>().isActivated = false;
            leftTrigger.GetComponent<Grimoire_trigger>().isActivated = false;
            activatedFirst = "none";
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "indexTip")
        {
            leftTrigger.GetComponent<Grimoire_trigger>().isActivated = false;
            rightTrigger.GetComponent<Grimoire_trigger>().isActivated = false;
        }
    }
}
