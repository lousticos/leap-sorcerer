﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draw : MonoBehaviour
{
    public GameObject trailPrefab;
    public GameObject pierreCollider;
    public GameObject trailInstancer;
    public float trailHeight = 0.004f;

    private bool drawing = false;
    private GameObject thisTrail;
    private Color trailColor = Color.white;
    private int layerValue = 10;

    void SetTrailColor()
    {
        if (GetComponent<Brush>().cyan && !GetComponent<Brush>().yellow && !GetComponent<Brush>().magenta)
            trailColor = Color.cyan;

        if (!GetComponent<Brush>().cyan && !GetComponent<Brush>().yellow && GetComponent<Brush>().magenta)
            trailColor = Color.magenta;

        if (!GetComponent<Brush>().cyan && GetComponent<Brush>().yellow && !GetComponent<Brush>().magenta)
            trailColor = Color.yellow;

        if (!GetComponent<Brush>().cyan && GetComponent<Brush>().yellow && GetComponent<Brush>().magenta)
            trailColor = Color.red;

        if (GetComponent<Brush>().cyan && GetComponent<Brush>().yellow && !GetComponent<Brush>().magenta)
            trailColor = Color.green;

        if (GetComponent<Brush>().cyan && !GetComponent<Brush>().yellow && GetComponent<Brush>().magenta)
            trailColor = Color.blue;

        if (GetComponent<Brush>().cyan && GetComponent<Brush>().yellow && GetComponent<Brush>().magenta)
            trailColor = Color.black;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == pierreCollider)
        {
            if (GetComponent<Brush>().brushEnabled)
            {
                SetTrailColor();
                Gradient gradient = new Gradient();
                thisTrail = (GameObject)Instantiate(trailPrefab, transform.position, Quaternion.identity, trailInstancer.GetComponent<Transform>());
                thisTrail.GetComponent<Trail_script>().cyan = GetComponent<Brush>().cyan;
                thisTrail.GetComponent<Trail_script>().magenta = GetComponent<Brush>().magenta;
                thisTrail.GetComponent<Trail_script>().yellow = GetComponent<Brush>().yellow;
                thisTrail.GetComponent<TrailRenderer>().material = new Material(Shader.Find("Sprites/Default"));
                gradient.SetKeys(
                    new GradientColorKey[] { new GradientColorKey(trailColor, 0.0f), new GradientColorKey(trailColor, 1.0f) },
                    new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 1.0f) }
                        );
                thisTrail.GetComponent<TrailRenderer>().colorGradient = gradient;
                thisTrail.GetComponent<TrailRenderer>().sortingOrder = layerValue;
                drawing = true;
            }
        }
    }

   /* void OnTriggerStay(Collider other)
    {
        if (GetComponent<Brush>().brushEnabled)
        {
            if (other.gameObject == pierreCollider)
            {
                if (thisTrail)
                {
                    thisTrail.transform.position = new Vector3(transform.position.x, trailHeight, transform.position.z);
                }
            }
        }
    }*/

    void Update()
    {
        if(drawing)
        {
            if (thisTrail)
            {
                thisTrail.transform.position = new Vector3(transform.position.x, trailHeight, transform.position.z);
            }
        }
        else
        {

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == pierreCollider)
        {
            if (thisTrail)
            {
                layerValue++;
                drawing = false;
            }
        }
    }
}