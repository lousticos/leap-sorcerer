﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailDetector : MonoBehaviour
{
    internal bool isTriggered = false;

    public bool cyan = false;
    public bool magenta = false;
    public bool yellow = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "trail")
        {
            isTriggered = true;
            cyan = other.GetComponent<Trail_script>().cyan;
            magenta = other.GetComponent<Trail_script>().magenta;
            yellow = other.GetComponent<Trail_script>().yellow;
        }
    }
}
