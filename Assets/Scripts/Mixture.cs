﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mixture : MonoBehaviour
{

    public GameObject ownLiquid;
    public bool cyan = false;
    public bool magenta = false;
    public bool yellow = false;

    void Update()
    {
        if(!cyan && !magenta && !yellow)
        {
            GetComponent<Renderer>().enabled = false;
        }
        else
        {
            GetComponent<Renderer>().enabled = true;
        }
        if(cyan)
        {
            if(yellow)
            {
                if(magenta)
                {
                    GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0.5f);
                }
                else
                {
                    GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                }
            }
            else
            {
                if (magenta)
                {
                    GetComponent<Renderer>().material.color = new Color(0, 0, 1, 0.5f);
                }
                else
                {
                    GetComponent<Renderer>().material.color = new Color(0, 1, 1, 0.5f);
                }

            }
        }
        else
        {
            if (yellow)
            {
                if (magenta)
                {
                    GetComponent<Renderer>().material.color = new Color(1, 0, 0, 0.5f);
                }
                else
                {
                    GetComponent<Renderer>().material.color = new Color(1, 1, 0, 0.5f);
                }
            }
            else
            {
                if (magenta)
                {
                    GetComponent<Renderer>().material.color = new Color(1, 0, 1, 0.5f);
                }
                else
                {
                    GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.5f);
                }
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        string otherColor = "none";
        if (other.gameObject.tag=="fluid" && other.GetComponent<Renderer>().enabled)
        {
            otherColor = other.GetComponent<Fluid>().color;
        }
        switch(otherColor)
        {
            case "cyan":
                cyan = true;
                break;
            case "magenta":
                magenta = true;
                break;
            case "yellow":
                yellow = true;
                break;
            default:
                break;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == ownLiquid)
        {
            cyan = false;
            magenta = false;
            yellow = false;
        }
    }
}
