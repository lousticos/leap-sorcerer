﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grimoire_trigger : MonoBehaviour
{
    public bool isActivated = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "indexTip")
        {
            isActivated = true;
        }
    }
}
