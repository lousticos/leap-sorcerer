ATTENTION : POUR QUE LES MAINS DU JOUEUR SOIENT DETECTEES CORRECTEMENT CELUI CI DOIT LES PLACER A UNE VINGTAINE DE CENTIMETRE AU DESSUS DU BOITIER DU LEAP MOTION

Utilisation du Grimoire

Glisser le doigt sur le grimoire de droite � gauche pour aller � la page suivante et de gauche � droite pour aller � la page pr�c�dente.

Utilisation des potions

Fermer la main pour saisir une fiole de potion.
Incliner la fiole pour faire couler le liquide.
Faire couler le liquide dans le bol pour le remplir.
Faire couler le liquide dans un bol d�j� remplis m�lange les couleurs.
Incliner le bol pour le vider.

Utilisation de la pierre d�incantation

Tremper le doigt dans le bol pour passer en mode dessin.
Une fois en mode dessin, toucher la pierre pour dessiner � l�endroit touch�.
Toucher l��ponge avec le doigt pour le nettoyer et quitter le mode dessin.
Fermer la main pour saisir l��ponge.
Toucher la pierre avec l��ponge pour effacer le dessin trac�.


Voil� pour les contr�les, pour ce qui est du jeu, laissez vous guider par le grimoire�!